'use strict'

module.exports = {
  tcpStreamerPort: process.env.TCP_PORT || 3334,
  streamerCheckerPort: process.env.CHECKER_PORT || 3335
}
