'use strict'

const server = require('./server')
const winston = require('winston')

const logger = winston.createLogger({
  transports: [new winston.transports.Console({ timestamp: true })]
})

// Connect Zeromq Sockets
server.connect(err => {
  if (err) {
    logger.log({
      level: 'info',
      message: err
    })
    process.exit(1)
  }
})

server.event.on('info', message => {
  logger.log({
    level: 'info',
    message: message
  })
})

server.event.on('error', err => {
  logger.log({
    level: 'error',
    message: err
  })
  process.exit(1)
})

process.on('uncaughtException', err => {
  logger.log({
    level: 'error',
    message: `uncaughtException: ${err}`
  })
  process.exit(1)
})
