'use strict'

const EventEmitter = require('events').EventEmitter
const zmq = require('zeromq/v5-compat')
const ports = require('./ports')

// tcp -> streamer -> checker
const tcpPull = zmq.socket('pull')
const checkerPush = zmq.socket('push')

const event = new EventEmitter()

const connect = done => {
  // tcp -> streamer
  tcpPull.bind(`tcp://0.0.0.0:${ports.tcpStreamerPort}`, err => {
    if (err) return done(err)
    event.emit(
      'info',
      `Socket PULL from tcp listen on ${ports.tcpStreamerPort}`
    )
  })
  // streamer -> checker
  checkerPush.bind(`tcp://0.0.0.0:${ports.streamerCheckerPort}`, err => {
    if (err) return done(err)
    event.emit(
      'info',
      `Socket PUSH to checker listen on ${ports.streamerCheckerPort}`
    )
  })

  // TCP -> CHECKER
  tcpPull.on('message', (...data) => {
    checkerPush.send(data)
    event.emit('info', `New data from tcp to checker -> ${data}`)
  })

  done()
}

module.exports = {
  event: event,
  connect: connect
}
