'use strict'

const expect = require('chai').expect
const connect = require('../src/server').connect
const zmq = require('zeromq/v5-compat')
const ports = require('../src/ports')

describe('streamer', function () {
  this.timeout(20000)

  before(done => {
    connect(err => {
      if (err) throw err
      done()
    })
  })

  it('Should receive data in checker from tcp', done => {
    const push = zmq.socket('push')
    const pull = zmq.socket('pull')

    push.connect(`tcp://localhost:${ports.tcpStreamerPort}`)
    pull.connect(`tcp://localhost:${ports.streamerCheckerPort}`)
    push.send(['from tcp', 'to checker'])
    pull.on('message', (type, data) => {
      expect(type).to.eql(Buffer.from('from tcp'))
      expect(data).to.eql(Buffer.from('to checker'))
      push.disconnect(`tcp://localhost:${ports.tcpStreamerPort}`)
      pull.disconnect(`tcp://localhost:${ports.streamerCheckerPort}`)
      done()
    })
  })
})
